'''

Capstone Specifications:

1. Create a Person class that is an abstract class that has the following methods:

    a. getFullName method
    b. addRequest method
    c. checkRequest method
    d. addUser method

2. Create an Employee class from Person with the following properties and methods:

    a. Properties(make sure they are private and have getters/setters)

    firstName, lastName, email, department

    b. Methods:
    Abstract methods: 
    # For the checkRequest and addUser methods, they should do nothing. 

    login() - outputs "<Email> has logged in"
    logout() - outputs "<Email> has logged out"

    # Note: All methods just return Strings of simple text
        ex. Request has been added

3. Create a TeamLead class from Person with the following properties and methods:

    a. Properties(make sure they are private and have getters/setters)

    firstName, lastName, email, department, members

    b. Methods:
    Abstract methods:
    # For the addRequest and addUser methods, they should do nothing.

    login() - outputs "<Email> has logged in"
    ogout() - outputs "<Email> has logged out"
    addMember() - adds an employee to the members list

    # Note: All methods just return Strings of simple text
        ex. Request has been added

4. Create an Admin class from Person with the following properties and methods:

    a. Properties(make sure they are private and have getters/setters)

    firstName, lastName, email, department

    b. Methods:
    Abstract methods:

    # For the checkRequest and addRequest methods, they should do nothing.

    login() - outputs "<Email> has logged in"
    logout() - outputs "<Email> has logged out"
    addUser() - outputs "New user added"

    # Note: All methods just return Strings of simple text
        ex. Request has been added

5. Create a Request that has the following properties and methods:

    a. properties
    name, requester, dateRequested, status

    b. Methods:
    updateRequest
    closeRequest
    cancelRequest

    # Note: All methods just return Strings of simple text 
        ex. Request < name > has been updated/closed/cancelled


'''

# [SOLUTION]

from abc import ABC, abstractmethod

class Person(ABC):
    def __init__(self):
        self.requests = []
        self.users = []

    @abstractmethod
    def getFullName(self):
        pass

    @abstractmethod
    def login(self):
        pass

    @abstractmethod
    def logout(self):
        pass

    @abstractmethod
    def setEmail(self, email):
        pass

    @abstractmethod
    def getEmail(self):
        pass

    @abstractmethod
    def setDepartment(self, department):
        pass

    @abstractmethod
    def getDepartment(self):
        pass

    @abstractmethod
    def setFirstName(self, firstName):
        pass

    @abstractmethod
    def getFirstName(self):
        pass

    @abstractmethod
    def setLastName(self, lastName):
        pass

    @abstractmethod
    def getLastName(self):
        pass

    def addRequest(self, request):
        self.requests.append(request)
        print(f"Request {request} has been added")

    def checkRequest(self):
        print("Request has been checked")

    def addUser(self, user):
        self.users.append(user)
        print("User has been added")

class Employee(Person):
    def __init__(self):
        super().__init__()
        self._firstName = ""
        self._lastName = ""
        self._email = ""
        self._department = ""

    def getFirstName(self):
        return self._firstName

    def setFirstName(self, firstName):
        self._firstName = firstName

    def getLastName(self):
        return self._lastName

    def setLastName(self, lastName):
        self._lastName = lastName

    def getEmail(self):
        return self._email

    def setEmail(self, email):
        self._email = email

    def getDepartment(self):
        return self._department

    def setDepartment(self, department):
        self._department = department

    def getFullName(self):
        return f"{self._firstName} {self._lastName}"

    def login(self):
        print(f"{self._email} has logged in")

    def logout(self):
        print(f"{self._email} has logged out")

class TeamLead(Person):
    def __init__(self):
        super().__init__()
        self._firstName = ""
        self._lastName = ""
        self._email = ""
        self._department = ""
        self.members = []

    def getFirstName(self):
        return self._firstName

    def setFirstName(self, firstName):
        self._firstName = firstName

    def getLastName(self):
        return self._lastName

    def setLastName(self, lastName):
        self._lastName = lastName

    def getEmail(self):
        return self._email

    def setEmail(self, email):
        self._email = email

    def getDepartment(self):
        return self._department

    def setDepartment(self, department):
        self._department = department

    def getFullName(self):
        return f"{self._firstName} {self._lastName}"

    def login(self):
        print(f"{self._email} has logged in")

    def logout(self):
        print(f"{self._email} has logged out")

    def addMember(self, member):
        self.members.append(member)
        print(f"Member {member} has been added")

class Admin(Person):
    def __init__(self):
        super().__init__()
        self._firstName = ""
        self._lastName = ""
        self._email = ""
        self._department = ""

    def getFirstName(self):
        return self._firstName

    def setFirstName(self, firstName):
        self._firstName = firstName

    def getLastName(self):
        return self._lastName

    def setLastName(self, lastName):
        self._lastName = lastName

    def getEmail(self):
        return self._email

    def setEmail(self, email):
        self._email = email

    def getDepartment(self):
        return self._department

    def setDepartment(self, department):
        self._department = department

    def getFullName(self):
        return f"{self._firstName} {self._lastName}"

    def login(self):
        print(f"{self._email} has logged in")

    def logout(self):
        print(f"{self._email} has logged out")

    def addUser(self, user):
        self.users.append(user)
        print("New user added")

class Request:
    def __init__(self, name, requester, dateRequested):
        self.name = name
        self.requester = requester
        self.dateRequested = dateRequested
        self.status = "Open"

    @abstractmethod
    def updateRequest(self):
        pass

    @abstractmethod
    def closeRequest(self):
        pass

    @abstractmethod
    def cancelRequest(self):
        pass

class ConcreteRequest(Request):
    def updateRequest(self):
        self.status = "Updated"
        print(f"Request {self.name} has been updated")

    def closeRequest(self):
        self.status = "Closed"
        print(f"Request {self.name} has been closed")

    def cancelRequest(self):
        self.status = "Cancelled"
        print(f"Request {self.name} has been cancelled")


# [ TEST CASES ]
# Note: other part of this test section have been commented out for capstone sample output.


employee = Employee()
employee.setFirstName("Robert")
employee.setLastName("Patterson")
employee.setEmail("robert.patterson@example.com")
employee.setDepartment("HR")


#employee.addRequest("Vacation request")
#employee.addUser("New employee")


#employee.login()
#employee.logout()


print(employee.getFullName())
#print("Department:", employee.getDepartment())


team_lead = TeamLead()
team_lead.setFirstName("Brandon")
team_lead.setLastName("Smith")
team_lead.setEmail("brandon.smith@example.com")
team_lead.setDepartment("Engineering")


print(team_lead.getFullName())
#print("Department:", team_lead.getDepartment())


admin = Admin()
admin.setFirstName("Admin")
admin.setLastName("User")
admin.setEmail("admin@example.com")
admin.setDepartment("Management")


#admin.addUser("New user")


#print("Full Name:", admin.getFullName())
#print("Department:", admin.getDepartment())

request = ConcreteRequest("Capstone Approval", "Jane Doe", "2023-09-01")

# request.updateRequest()
request.closeRequest()
# request.cancelRequest()
